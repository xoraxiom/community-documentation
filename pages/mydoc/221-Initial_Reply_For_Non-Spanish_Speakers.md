---
title: Initial Reply - For Non-Spanish Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Spanish speakers"
sidebar: mydoc_sidebar
permalink: 221-Initial_Reply_For_Non-Spanish_Speakers.html
folder: mydoc
conf: Public
lang: es
---


# Initial Reply - For Non-Spanish Speakers
## First response, Email to Client if you're a non-Spanish speaker

### Body


Hola {{ beneficiary name }}:

Mi nombre es {{ incident handler name }}, soy parte del equipo de la Línea de Ayuda en Seguridad Digital de Access Now - https://accessnow.org/help.

Hemos recibido su mensaje {{ email subject }}. Lastimosamente yo no hablo español fluido, si esta es una cuestión urgente, por favor responder a este mensaje agregando la etiqueta “URGENT” en el asunto y le atenderemos lo mas pronto posible.

Si usted puede continuar la conversación en Inglés o [[ Other Languages ]] [[ en ]] inglés  [[ fr ]] francés [[ de ]] alemán [[ ar ]] árabe con gusto podré responder a sus preguntas directamente.

Nuestros compañeros y nuestras compañeras hispanohablantes estarán en turno dentro de unas horas para darle seguimiento al caso.

Cordialmente,

{{ incident handler name }}


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
