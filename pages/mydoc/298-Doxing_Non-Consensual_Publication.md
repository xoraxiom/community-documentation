---
title: Doxing and Non-Consensual Publication of Pictures and Media
keywords: harassment, gendered online violence, doxing, doxxing, non-consensual publication, pictures, media, personal information, OSINT, open source intelligence
last_updated: October 7, 2021
tags: [harassment, articles]
summary: "A civil society member's personal details, pictures, or other private information have been published online."
sidebar: mydoc_sidebar
permalink: 298-Doxing_Non-Consensual_Publication.html
folder: mydoc
conf: Public
lang: en
---


# Doxing and Non-Consensual Publication of Pictures and Media
## How to deal with a case of doxing or non-consensual publication of personal information against a civil society member

### Problem

Doxing, or doxxing, consists in publishing an individual's personal details, for example their physical address or official identity.

Non-consensual sharing of private content consists in publishing media and pictures, often nude or compromising, relating to someone's private life. Media often label this attack as "revenge porn" - a term which we should avoid, as it highlights the attackers' point of view.

For civil society members in particular, these kinds of attacks may lead to the following consequences:

- The incident could lead to digital, reputational, physical or psychological damage to the targeted person.
- The suffered damage could stop the survivor from working, silence them, or compromise the activities of their entire organization.
- In some countries, outing of a person could lead to their arrest.
- The posted documents may lead to other kinds of harassment, like attacks by mobs or blackmailing.


* * *


### Solution

#### How we should deal with the requester

When someone has been doxed or their pictures or other information have been published without their consent, they are often in a very delicate emotional state, so we should put them at ease, treat them seriously, and ensure they know that we believe them and are on their side.

For more recommendations on how to deal with these cases, what questions to ask, and what basic measures to recommend, see [Article #234: Online Harassment Targeting a Civil Society Member](234-FAQ-Online_Harassment.html).


#### Removal of content

There are many ways to take down the content, although sometimes attackers open new accounts with new names. In case of non-consensual sharing of pornographic content, it has proved useful to use the official forms of the websites hosting this content, send emails to their domain's company, or write to their social media accounts.

On social media, doxing and non-consensual sharing of private content have similar solutions with similar processes:

- **Without My Consent**: [Without My Consent](http://www.withoutmyconsent.org/resources) is a non-profit organization seeking to combat online invasions of privacy. Their website explains what to do in case someone has distributed nude photos or videos of the client online, without their consent or in breach of their trust.

- **Cyber Civil Rights Initiative**: [Online removal guide](https://www.cybercivilrights.org/online-removal/)

- **Blogger**: [“Someone is posting my private information or explicit photos or videos of me without my consent”](https://support.google.com/blogger/contact/private_info?id=&amp;url=)

In case of doxing, see also EFF Director of Cybersecurity Eva Galperin's recommendations
on [What to Do if You've Been Doxed](https://www.wired.com/story/what-do-to-if-you-are-being-doxed/).

To be alerted about non-consensual sharing of private content, we might recommend the client to set up a [Google alert](https://www.google.com/alerts) to be informed if something is published with their names.
#### Content on websites

If the harasser is using a website/server, such as to send malicious emails or to upload harassing content, the client may need to contact the administrators or the hosting provider in order to stop or mitigate the attack.

The Helpline team can help identify the contact point using WHOIS to identify the server/website owner.


#### Inform the police

In some cases the targeted person may need to inform the police. The Helpline should assist the client to determine whether this would help or further damage their situation. Especially if the adversary is a part of or is linked to the government, contacting the authorities could only make things worse.

If the website/server used by the harasser does not accept to collaborate, the targeted person may need to contact the authorities of the host country to take down the harmful content.


* * *


### Comments

For more resources and information on organizations we can refer the client to, see  [Article #234: Online Harassment Targeting a Civil Society Member](234-FAQ-Online_Harassment.html).

For preventing doxing, for example in preparation of online advocacy campaigns on controversial topics, we can recommend the client to research what information on them is available online and could be used against them. There are many tutorials for this kind of research, which is generally called "self-doxing", but most of them are US-centered and include only search engines for US-based people.

- If the client is not based in the US, we can share with them [the Helpline's end-user guide on self-doxing](https://guides.accessnow.org/self-doxing.html).
- If the client is based in the US, we can share with them Equality Labs' [Self-Doxing guide](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
  and this detailed [Personal Data Removal Workbook &amp; Credit Freeze Guide](https://inteltechniques.com/data/workbook.pdf)


* * *


### Related Articles

- [Article #234: Online Harassment Targeting a Civil Society Member](234-FAQ-Online_Harassment.html).
