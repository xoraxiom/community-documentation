---
title: Safe Browsing Practices and Plugins
keywords: browser, privacy, Firefox, Chrome, HTTPS Everywhere, uBlock Origin, NoScript, Privacy Badger, Tor, Tor Browser, VPN, addons, extensions
last_updated: February 2021
tags: [browsing_security, articles]
summary: "A client is inquiring about best practices when browsing online."
sidebar: mydoc_sidebar
permalink: 212-Safe_Browsing_Practices.html
folder: mydoc
conf: Public
lang: en
---

# Safe Browsing Practices and Plugins
## Best practices, tools, and application that can provide a more secure browsing experience for Helpline clients

### Problem

Online users have all experienced issues with adware, pop-ups, viruses, and other malicious content from the web. Tracking users and censorship over a specific region or country are also common practices. These threats may make our users feel unsafe. The following solutions will address the majority of their concerns.

These include the following:

- Protection from:
    - Adware/Popups
    - Scripts, malware and drive-by downloads
    - Web/URL redirects
    - Browser changes (settings, home pages, bookmarks, add-ons)

- Providing:
    - Protection from tracking
    - Anonymity
    - Censorship


* * *


### Solution

#### Choosing a Browser

There are many browsers available, and depending on our client's needs, they will have multiple options to choose from. Keep in mind that the client's context will have an effect on their options, as well: for example, in some countries it may be illegal to use Tor Browser to circumvent censorship.

- [Tor Browser](https://www.torproject.org/)
    - Blocks tracking and fingerprinting, hides the user's IP, and allows access to onion sites.
    - Can help a user circumvent censorship by an ISP or another actor.
    - Tor Browser comes with some browser extensions pre-installed. Users should be cautioned against installing extra extensions and add-ons, as [doing so may weaken Tor Browser's privacy protections](https://support.torproject.org/#faq-3).
    - To get the most out of Tor Browser, the user may need to [change some of their browsing habits](https://support.torproject.org/#staying-anonymous).
- [Brave](https://brave.com/)
    - Has built-in protections against tracking and scripts, and automatically blocks ads.
    - Protects against device fingerprinting.
    - More information on Brave's privacy protections [here](https://brave.com/privacy-features/).
- [Firefox](https://www.mozilla.org/en-US/firefox/browsers/)
    - Has built-in protections against tracking.
    - Allows users to open sites in 'container tabs,' which can help prevent the site from creating a user profile based on the user's activity in other browser tabs.
    - With Firefox, we may want to recommend changing the default settings to increase privacy. Clients can disable telemetry sharing, disable search and URL autocomplete, and make sure not to login to a Firefox account in the browser or enable cross-device syncing.
- [Google Chrome](https://www.google.com/chrome/) or another Chromium-based browser
    - Sends a persistent identifier back to Google along with visited website addresses, allowing the user's identity to be linked to their browsing activity.
    - If a user needs to use Chrome, we should recommend that they disable search and URL autocomplete in their settings.

**A user who prioritizes privacy should consider:**
   - Tor Browser
   - Brave
   - Firefox (with tweaked settings)


#### Securing the Browser

1. It is always recommended to check if the client's browser is up-to-date. This can be done by visiting [this website](https://browsercheck.qualys.com). If an update is required, the client can try fixing the issue by clicking the “FIX IT” button in the web page with the results. This will help the client download or install the latest version for their browser or plugins.

2. Force HTTPS on the browser by installing the [HTTPS Everywhere extension](https://www.eff.org/https-everywhere).

3. Block ads and scripts:
    - uBlock Origin - Available in the [Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) and [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) Webstores
    - NoScript for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/noscript/) - *please note that this extension can dramatically change the browser experience and probably should only be used by high-risk users in addition to other safety practices.*

4. Avoid tracking:
    - [Privacy Badger](https://www.eff.org/privacybadger)
    - [Disconnect](https://disconnect.me/disconnect)

5. Anonymity (for more information and the risks implied, see [Article #175: FAQ - Circumvention & Anonymity tools list](175-Circumvention_Anonymity_tools_list.html):
    - Tor Browser (https://www.torproject.org)

6. Other tools:
    - [Web of Trust](https://www.mywot.com) - a free browser extension, mobile app and API that lets the user check if a website is safe before they visit it.

7. Browsing Best Practices & Browser Plugins Review:
    - Always make sure that the browser you use is updated along with its extensions.
    - Always review the browser plugins you have installed, and remove unnecessary add-ons and plugins.
    - When possible, use a privacy-conscious search engine like [DuckDuckGo](https://html.duckduckgo.com/html/).

8. Review security hygiene with the user. Encourage them to continue to update their software, keep an eye out for suspicious links, and avoid downloading unsafe and unnecessary content.


* * *


### Comments

- [Web Browser Privacy: What Do Browsers Say When They Phone Home?](https://www.scss.tcd.ie/Doug.Leith/pubs/browser_privacy.pdf) - Douglas J. Leith, February 24th, 2020.
- [Study ranks the privacy of major browsers. Here are the findings](https://arstechnica.com/information-technology/2020/03/study-ranks-edges-default-privacy-settings-the-lowest-of-all-major-browsers/) - Ars Technica, March 17th, 2020.
