---
title: Response to 1st-Time/Non-Vetted Helpline Client
keywords: case handling policy, initial reply, non-vetted
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First reply - questions and introduction to vetting"
sidebar: mydoc_sidebar
permalink: 256-response_first-time_client.html
folder: mydoc
conf: Public
lang: en
---


# Response to 1st-Time/Non-Vetted Helpline Client
## First reply - questions and introduction to vetting

### Body

Dear {{ beneficiary name }},

Thanks for reaching out to Access Now Digital Security Helpline, again this is {{ incident handler name }}. We appreciate your trust in our Helpline service. Access Now's Digital Security Helpline is a free of charge resource for civil society around the world. We offer real-time, direct technical assistance and advice to civil society groups & activists, media organizations, journalists & bloggers, and human rights defenders.

We are happy to assist, but first we have some questions for you:

1. How did you find out about us? Were you referred by someone? If so, can you tell us who?

2. Please tell us about what you do. Do you belong to any organization or group?

3. If available, can you cite website links that show your work in the civil society space and human rights movement?

4. We would be grateful if you could tell us what name and pronoun/s you would like to be addressed with.


* * *

We would like to inform you that as part of our security measures, we vet new potential Helpline clients. This means we reach out to trusted contacts and share some information like your name, e-mail address and organization you belong to. Please be assured that the reason why you are contacting us and any information related to your case will be kept strictly confidential and only available to the Helpline team.

To move this process along, it will be helpful if you can provide us reference persons to vouch for you (two names and their contact details at least), and by filling out the following survey about you and the scope of your work:

https://form.accessnow.org/index.php/652353/Y?ticket={{ vetting ticket id }}

If you do not wish that we go through this process, please let us know within 48 hours and we will try find an alternative way to verify.

* * *

We understand that the data we are requesting may be sensitive and confidential, so we should establish a secure channel of communications. If you already use PGP, please send us your public key. Otherwise please let us know if you use any other secure communication channel, like Signal Messenger, Wire.com or XMPP/OTR.

If you aren't familiar with any of these tools, we will be glad to guide you in setting up one of them to communicate with us securely.

* * *

To part, please note that Access Now’s Helpline services are not intended for individuals under the age of 18. If you are underage, please do let us know so that we can direct you to partners that can provide the support your require.

Kind Regards,
{{ incident handler name }}


### Comments

This template is editable, and must be adapted for use depending on the context of the email sent to us by the client.

In case of gendered online violence, please see [Article #268: Initial Reply for Harassment Cases](268-Initial_Reply_Harassment.html.html).


* * *


### Related Articles


- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
- [Article #268: Initial Reply for Harassment Cases](268-Initial_Reply_Harassment.html)
