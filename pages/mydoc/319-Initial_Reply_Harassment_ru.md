---
title: Initial Reply for Harassment Cases - Russian
keywords: email templates, initial reply, case handling policy, harassment, gendered online violence
last_updated: August 12, 2021
tags: [helpline_procedures_templates, harassment_templates, templates]
summary: "Первый ответ клиенту - жертве гендерно-ориентированной онлайновой агрессии"
sidebar: mydoc_sidebar
permalink: 319-Initial_Reply_Harassment_ru.html
folder: mydoc
conf: Public
ref: Initial_Reply_Harassment
lang: ru
---


# Initial Reply for Harassment Cases
## First response to clients who have been targeted by gendered online violence

### Body

Здравствуйте,

Спасибо, что обратились в Службу поддержки по вопросам цифровой безопасности организации Access Now (https://www.accessnow.org/help). Меня зовут {{ incident handler name }}. Я помогу вам разобраться с проблемой и решить, что делать дальше.

Мы получили ваш первоначальный запрос, и я работаю над ним. Чтобы наилучшим образом учесть ваши потребности и понять произошедшее, мы просим вас ответить на несколько вопросов:

- Будет ли для вас удобно, если мы поговорим по видеосвязи или хотя бы голосом?
- Пожалуйста, сообщите, если хотите, чтобы вас представлял кто-то другой. Мы можем общаться с тем, кому вы доверяете, кто знает детали вашего дела и готов переписываться по электронной почте.
- Если вы предпочитаете говорить с кем-то женского пола, пожалуйста, дайте мне знать, и я приглашу кого-нибудь из коллег.

До нашего разговора я прошу и вас помочь мне. Пожалуйста, фиксируйте то, что происходит. Сохраняйте наиболее важную информацию. Найдите друга, которому можете доверять, с которым вам спокойно и который поможет вам в нынешней ситуации.

Пожалуйста, ответьте на мои вопросы, сохранив "[accessnow #{{ ticket id }}]" в поле темы письма.

С уважением,

{{ incident handler name }}


* * *


### Related Articles

- [Article #234: Online Harassment Targeting a Civil Society Member](234-Online_Harassment_Against_Civil_Society_Member.html)
