---
title: Inform Intermediary About Vetting Process
keywords: email templates, vetting, vetting process, intermediaries
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Template to inform an intermediary about our vetting process"
sidebar: mydoc_sidebar
permalink: 196-Vetting_Info_Intermediaries.html
folder: mydoc
conf: Public
ref: Vetting_Info_Intermediaries
lang: en
---


# Inform Intermediary About Vetting Process
## Template to inform an intermediary about our vetting process

### Body

Hi {{ beneficiary name }},

We appreciate your trust in the Helpline. In order to protect the at-risk users we serve and extend our trust network, we vet potential Helpline clients.

During this process, we reach out to Access Now's trusted partners where we may share {{ beneficiary name }}'s name and contact information to (1) confirm they are who they state they are and (2) they are part of civil society. Any other information, including the reason they contacted our Helpline, will remain strictly confidential.

Please also note that Access Now’s Helpline services are not intended for individuals under the age of 18. If you are aware {{ beneficiary name }} is underage, please do let us know to direct them to partners that can provide the support they require.

Our team will continue to work on your request while we complete this process. If you have any questions or concerns, please let us know.

You can help us move this process forward by filling out the following survey about them and the scope of their work:

https://form.accessnow.org/index.php/754436/Y?ticket={{ vetting ticket id }}

Regards,

{{ incident handler name }}
