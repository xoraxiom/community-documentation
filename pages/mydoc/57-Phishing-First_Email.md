---
title: Phishing - First Email
keywords: email templates, phishing, email headers
last_updated: August 3, 2022
tags: [phishing_suspicious_email_templates, templates]
summary: "First email, with precautions and instructions to get full headers"
sidebar: mydoc_sidebar
permalink: 57-Phishing-First_Email.html
folder: mydoc
conf: Public
ref: Phishing-First_Email
lang: en
---


# Phishing - First Email
## First email, with precautions and instructions to get full headers

### Body

Hi {{ beneficiary name }},

Thanks for reaching out to us about the suspicious email you received.

[[ If the reasons why the email is suspicious are not clear ]]
I will start analyzing the message you received as soon as possible. However, if you could please clarify why the email is suspicious to you, that would be helpful.

First of all, it would be helpful to know if the supposed sender of the email
you received is someone you know. If they're someone you know, did the email appear to be sent from their email address, or was their name used but with a different email address?

[[ If the email contains attachments/links ]]
*Please do not open any link or attachment in the suspicious email.* Since this is a suspicious email, it's best to work with caution. With this in mind, please don't follow any link or download any attachment until we are able to confirm the veracity of this communication. If you've already opened any links or downloaded any attachments from the email, please let us know, and we can help you address that as well.

To perform a full analysis of the email, we'll need a copy of the suspicious email. In this link you can find detailed instructions on how to export email messages as a .eml file for different email services and clients: https://mediatemple.net/community/products/dv/360021670392/how-to-export-emails-as-a-(.eml)-file. If you need any help with this, please let me know.

I remain available if you have any doubts or questions. Please feel free to reach out to me if you need additional help.

With best regards,
{{ incident handler name }}

* * *

### Related Articles

- [Article #58: Client Receives a Suspicious/Phishing Email](58-Suspicious_Phishing_Email.html)
