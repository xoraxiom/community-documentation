---
title: Report URL miscategorization
keywords: URL, categorization, category, website management
last_updated: August 15, 2019
tags: [infrastructure, articles]
summary: "Access to a certain URL is being blocked by a network security device or software."
sidebar: mydoc_sidebar
permalink: 149-Report_URL_miscategorization.html
folder: mydoc
conf: Public
ref: Report_URL_miscategorization
lang: en
---


# Report URL miscategorization
## Guidelines on how to report miscategorized URLs

### Problem

- Access to a certain URL is being blocked by a network security device or software because it was categorized incorrectly (e.g. adult site, malicious site, etc).

- A newly introduced URL can be uncategorized or unclassified in the initial stages of its launch. Sometimes, due to absence of relevant information about the URL, there is a possibility of it getting blocked by web security filters and other similar technologies.

How to determine if a particular URL is miscategorized?

How to report a miscategorized URL?


* * *


### Solution

Network security filters (web security appliances, firewalls, antispam, etc.) can block access to a specific URL based on its reputation. In most cases, these filters also implement security rules or policies which will either allow or block access to a certain URL based on its category. Therefore, proper URL categorization is important to prevent or minimize false positives (URLs should not be blocked) and false negatives (should be blocked but are missed by filters).

There are websites which allow users to check the category of a URL and at the same time provide a tool to submit a request for URL reclassification (if the URL is determined to be miscategorized). 

These are sample websites which provide such services:

1. [Symantec](http://rulespace.com/swg-ratertool/tool.php) - allows to check URL category and submit request for URL reclassification

2. [FortiGard Center](http://www.fortiguard.com/ip_rep/) - allows to check the URL category and offers the opportunity to suggest a different category for a certain URL
 
3. [Trend Micro](http://global.sitesafety.trendmicro.com/feedback.php) - allows to check URL category and provides an option  to request a URL reclassification

*Please note that these websites may ask for an email address to notify about the update of the request.*

It is also good to note that miscategorized URLs may have impact not just on the reputation of the website itself, but on its owner as well.


* * *


### Comments



* * *


### Related Articles

