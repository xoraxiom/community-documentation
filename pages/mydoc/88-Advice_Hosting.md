---
title: Advice on Hosting
keywords: hosting, providers, secure hosting
last_updated: April 4, 2019
tags: [infrastructure, articles]
summary: "A client is looking for a web hosting provider and needs advice on how to choose."
sidebar: mydoc_sidebar
permalink: 88-Advice_Hosting.html
folder: mydoc
conf: Public
lang: en
---


# Advice on Hosting
## Recommendations on how to choose a trusted website hosting provider

### Problem

A client needs to move their existing website to a different hosting provider or is launching a new website and needs to find a hosting provider for it.

Several problems might be associated with this need:

- The website might be developed with a specific framework or CMS that may be outdated
- The client might need technical support
- The client might be located in a country where electronic payment is restricted 
- The budget might be limited
- The technical responsible of the association may be more comfortable with a specific hosting provider


* * *


### Solution

#### Assess the client's needs

We can base our interview to assess the client's needs on [this article by equalit.ie]( https://learn.equalit.ie/wiki/Responsible_Data_Forum_on_Hosting ).

In particular, we need to ask:

- Is the existing website static or based on a CMS?
- If the website is being developed from scratch, does the client need a CMS or can they consider a static website?
- What CMS is the website based on?
- Does the website need to be hosted in a country different than the client's?
- Does the client have specific legal needs?
- Does the client need technical assistance from the hosting provider?
- Should DDoS protection be included?
- Should the hosting provider offer specific communications channels (e.g. phone, GPG, chat, etc.)?
- Does the client need SSH access to the server?


#### Selection criteria

Once we have assessed the client's needs, we can suggest them the best options. We should always recommend more than one option, so that the client can decide what's the best for them.

We should base our recommendations on the client's needs and on the following criteria:

- Friendliness towards NGOs
- Customer service and responsiveness
- Costs
- Support of a specific programming language or CMS
- Country where the company is based
- Security features (hardened servers, SSL certificates, DDoS protection, 2-factor authentication for accessing the administration interface...)
- Managed or unmanaged hosting
- SSH access to server

For more details on how to select a hosting solution, read this article by eQualit.ie on [Choosing a hosting provider](https://learn.equalit.ie/wiki/Choose_a_hosting_provider), with criteria for choosing a provider, and a comparison between managing a dedicated server or a virtual private server, and creating a website on a hosted platform like Wordpress.com or with a shared hosting provider.


##### NGO-friendly hosting providers

- **[Virtual Road](https://www.qurium.org)** - A highly professional and NGO-friendly team. They have proved to be efficient regarding various attack mitigations. They have a partnership with OTF for funding secure hosting.
    - NGO-friendly
    - Free for NGOs
    - DDoS protection included
    - [Security-oriented](https://www.qurium.org/secure-hosting/)
    - Managed hosting
    - [Domain co-hosting for censored websites](https://www.qurium.org/services/domain-co-hosting/)

- **[GreenHost](https://greenhost.nl)** is an NGO-friendly hosting provider funded by OTF to provide rapid response and secure hosting.
    - Commercial company, with free services funded by OTF for NGOs (see ["Review and selection process and eligibility"](https://greenhost.net/products/review-and-selection-process-and-eligibility/))
    - [Rapid response tools and solutions](https://greenhost.net/products/rapid-response-services/products-and-solutions/)
    - Based in the Netherlands
    - Installation of Globaleaks for whistleblowing platforms
    - Advanced e-mail, calendaring, contacts, and document sharing is possible through Zimbra.
    - Linux Virtual Private Servers

- Equalit.ie's **[EqPress](https://equalit.ie/eqpress)** - In case the website belongs to a civil society non-profit organization and runs on WordPress, EqPress can be recommended as a solution, as they provide free secure hosting for WordPress-based sites. eQPress includes [Deflect's DDoS protection](https://deflect.ca) and technical assistance by the Deflect team.
    - Dedicated to NGOs (see Deflect's [eligibility criteria](https://docs.deflect.ca/en/latest/eligibility.html))
    - Free service
    - DDoS protection included
    - Security-oriented
    - For Wordpress-based websites
    - Based in Canada, with servers in several locations

- **[WebArch](http://www.webarch.net/)** - A multi-stakeholder co-operative which provides ethical and green web hosting, virtual servers and GNU/Linux sysadmin support services. They are responsive and support LUKS disk encryption.
    - Web hosting
    - Virtual Private Servers
    - WordPress, MediaWiki, Discourse and many other web applications
    - Colocation
    - Technical support
    - Based in the UK

- **[Orange Website](https://www.orangewebsite.com/company.php)** - an Iceland-based web hosting provider focused on freedom of speech.
    - Anonymous web hosting (they only ask for an email)
    - Accepts BitCoin
    - No logging
    - 2-factor authentication
    - Web hosting
    - Virtual Private Servers
    - Dedicated servers
    - DDoS protection

- **[GreenNet](https://www.greennet.org.uk/)** - A not-for-profit collective established in 1985, providing internet services, web design and hosting to supporters of peace, the environment and human rights.
    - Web hosting
    - All services include telephone and email support from 9:30am to 5:30pm Monday to Friday
    - Support for PHP, Perl, Ruby &amp; many other programming languages
    - FTP and SFTP access
    - Site statistics reporting system
    - WordPress and Drupal
    - Colocation
    - Virtual Private Servers
    - Cloud storage
    - Web design and development

- **[Electric Embers](https://electricembers.coop/services/)** provides technical services and know-how to people working to make the world more just and sustainable.    
    - Commercial service with discounted prices for individuals and non-profit organizations
    - Virtual Private Servers (10GB disk space, 10GB traffic)
    - multiple MySQL databases with PHPMyAdmin access
    - PHP and other open-source languages (Perl, Python, Ruby)
    - SSH Unix shell access
    - DNS hosting for one domain
    - web server logs
    - statistics (AWStats)
    - automated nightly backups
    - SFTP
    - Free SSL based on [Let's Encrypt](https://letsencrypt.org/)
    - WordPress

- **[Gaia Host Collective](https://www.gaiahost.coop/ )** - A worker-owned cooperative providing hosting services with a focus on environmental and social sustainability.
    - Web hosting
    - Virtual Private Servers
    - WordPress
    - Drupal


##### Commercial hosting providers

- **[Hetzner](https://www.hetzner.com)** - a German hosting company.
    - Web hosting
    - Managed servers
    - Colocation
    - Cloud storage
    - DDoS protection

- **[Digitalocean](https://www.digitalocean.com/)** delivers one of the best quality services.
    - Several solutions for web hosting and VPS
    - SSH access
    - 2-factor authentication

- **[Edis](https://www.edis.at/en/home/)** Austrian hosting company with servers located in many different countries.
    - Web hosting
    - Colocation
    - Virtual Private Servers

- **[Gandi](https://www.gandi.net)** is used by some of our partners. Its servers are based in France, which could be problematic given the current state of emergency in the country (See [Gandi's declaration on this](http://www.gandibar.net/post/France-puts-forward-a-new-surveillance-law)).
    - Web hosting
    - WordPress
    - Virtual Private Servers can be moved to [colocation in the USA](https://wiki.gandi.net/en/hosting/faq-fr-us)

- **[OVH](https://www.ovh.com)** French hosting company with data centers in France and Canada. Given the current state of emergency in France, if the client decides to use OVH we should encourage them to select a data center in Canada.
    - Web hosting
    - Virtual Private Servers
    - Dedicated servers
    - Cloud storage
    - DDoS protection
    - WordPress, Drupal, Joomla!
    - [Let's Encrypt](https://letsencrypt.org/)-based SSL included


##### Autonomous tech collectives

- **[Shelter.is](https://www.shelter.is/)** is an autonomous tech collective that provides secure and privacy-friendly infrastructure run with solidarity in mind rather than motivated by profit.
    - Free, donation-based service
    - WordPress web hosting
    - SSL by default

- **[Systemli](https://systemli.org)** -  a left network and tech collective that was founded in 2003. Their aim is to provide safe and trustworthy communication services. The project addresses political activists and people who have a particular need to protect their data. They protect users' data by encrypting all their servers and connections and by avoiding to retain unnecessary connection data. Therefore, in case of an unauthorized access, the data is protected.
    - Static websites
    - WordPress hosting
    - Dokuwiki
    - phpBB for forums
    - [Ticker for covering events](https://www.systemli.org/en/service/ticker.html)
    - Analytics
    - TLS/SSL for all websites

- **[Immerda.ch](https://www.immerda.ch/infos/webhosting.html)** - an autonomous tech collective based in Switzerland.
    - Free for activists
    - Web hosting
    - WordPress, MediaWiki, Photo Gallery, and other CMSs
    - SSL certificates
    - Domains
    - Based in Switzerland
    - Website only in German

- **[Tachanka](https://tachanka.org/wiki/PublicServices)** is an autonomous tech collective offering virtual private servers to activists.
    - Virtual Private Servers
    - Drupal
    - Technical support

- **[Autistici/Inventati](https://www.autistici.org)** is an autonomous tech collective that does not log visits to its servers and hosted websites. It hosts many websites of groups for social change and free communication. It offers free hosting for static websites and also manages a blogging platform called Noblogs - see [below](#blogs).
    - Anonymous web hosting (they only ask for an email)
    - Free service
    - For activists and groups for social change (See A/I's [manifesto](https://www.autistici.org/who/manifesto) and [policy](https://www.autistici.org/who/policy))
    - Only static websites
    - Italian organization, servers located in several countries (not in Italy or the US)
    - No logging
    - Analytics

- **[Mayfirst](https://mayfirst.org/en/member-benefits/)** - a collective providing internet services to its members.
    - shared hosting infrastructure
    - PHP, perl, python, ruby, and other common programming languages
    - MySQL, Postgres, SQLite
    - un-restricted SSH and Secure FTP access
    - Mailman
    - shared ownCloud server
    - Drupal, WordPress, Joomla!, Wikimedia, CiviCRM and other CMSs and databases
    - all servers run the Debian stable operating system
<a name="blogs">
</a>
##### Blogging platforms

If the client just needs a blog, they can open a blog or a site on one of the several blogging platforms, like [Wordpress.com](https://wordpress.com/), [Blogger](https://www.blogger.com), or [Medium](https://medium.com/).

If they prefer a non-commercial solution, and don't need to associate their own domain to their website, they can consider [Autistici/Inventati's](https://www.autistici.org) [Noblogs.org](https://noblogs.org/), or [Nadir's](https://www.nadir.org/) [Blackblogs](https://blackblogs.org/) (based in Germany). Both these platforms are open to activists and grassroots organizations and don't keep logs of connections both by the administrators and readers of the blogs. To open a blog on these platforms, an email address by an autonomous tech collective is needed (you can find the complete list [here](https://www.autistici.org/links).


##### Not recommended
- **[1984](www.1984.is)** - Ethical and green web hosting provider. On November 15th 2017 a very serious systems anomaly occurred in the hosting equipment of the 1984 Hosting Company. A large number of customer websites and email accounts had to be restored from security backups. The 1984 Hosting technical staff has been busy preparing a new hosting setup ever since, and operations are now normal. A report on the incident will be made available once a post-mortem enquiry has been completed. We should wait for the report to be published before we recommend this hosting provider again.
    - More details on the incident in [this article](http://www.icenews.is/2017/11/21/1984-icelands-largest-hosting-providers-disastrous-crash-left-10-000-websites-down-for-days/#axzz5564XKGim)


* * *


### Comments

#### Static or dynamic website?

For recommendations on the technology that can be used to build the website, see  [Article #184: Recommend CMS/Framework for NGOs](184-Recommend_CMS-Framework_for_NGOs.html).

#### Domain registration and renewal

For recommendations on domain registration and renewal, see [Article #353: Recommendations for Domain Registration and Disputes](353-Recommendations_Domain_Registration.html).

* * *


### Related Articles

- [Article #184: Recommend CMS/Framework for NGOs](184-Recommend_CMS-Framework_for_NGOs.html)
- [Article #353: Recommendations for Domain Registration and Disputes](353-Recommendations_Domain_Registration.html)
