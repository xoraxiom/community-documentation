---
title: Initial Reply - For Arabic Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Arabic speakers"
sidebar: mydoc_sidebar
permalink: 274-Initial_Reply_For_Arabic_Speakers.html
folder: mydoc
conf: Public
lang: ar
---


# Initial Reply
## First response, Email to Client for Arabic speakers

### Body


مرحبا {{ beneficiary name }},

شكرا لمراسلتك لفريق مساعدو الأمن الرقمي بأكساس ناو
,(https://www.accessnow.org/help-ar/?ignorelocale)
 أنا إسمي
{{ incident handler name }},
و أنا هنا لتقديم المساعدة اللازمة لك.

يرجى العلم بأن فريق مساعدو الأمن الرقمي مدار بقبل مجموعة من المتخصيصين في الأمن الرقمي يشتغلون ضمن مناطق ذات فارق في التوقيت. لذلك أعضاء أخرون من الفريق قد يتجاوبو مع مراسلتك إعتمادا على التوقيت و اليوم الذي راسلتنا فيه. سنتابع أنا أو زميل من زملائي حول مراسلتك في وقت قصير حتى نستطيع أن نطلب منك تفاصيل إضافية أو سياق تكميلي إذا احتجنا لكي نستطيع العمل على طلبك.

يرجى مواصلة الأستحفاظ على
“[accessnow #{{ ticket id }}]"
في خانة  الموضوع  لكل المحاورات المستقبلية حول هاته المحادثة.  هذا رقم المرجع  المولد من طرف نظام ترتيب الرسائل الخاص بنا حتى نتمكن من تنظيم و متابعة المهام المتعلقه بطلبك.

أطيب التحيات،
{{ incident handler name }}


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
