---
title: Inform Clients of Vetting Process - Russian
keywords: email templates, vetting, vetting process
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Шаблон для информирования клиентов о нашей процедуре проверки"
sidebar: mydoc_sidebar
permalink: 335-Vetting_Info_Clients_ru.html
folder: mydoc
conf: Public
ref: Vetting_Info_Clients
lang: ru
---

дукт
# Inform Clients of Vetting Process
## Template to inform clients about our vetting process

### Body

Здравствуйте,

Спасибо, что доверяете нашей Службе поддержки. Для защиты пользователей, находящихся в уязвимом положении, и для расширения сети доверия мы проверяем потенциальных клиентов Службы поддержки.

Мы связываемся с доверенными партнерами Access Now и можем сообщить им ваше имя и электронный адрес. Мы хотим удостовериться, что вы действительно гражданский активист. Вся прочая информация, включая причину обращения в нашу службу поддержки, останется строго конфиденциальной.

Пожалуйста, обратите также внимание на то, что служба поддержки Access Now не предназначена для лиц младше 18 лет. Если вам еще нет 18 лет, просим сообщить нам об этом. Мы свяжем вас с партнерами, которые смогут предоставить вам необходимую поддержку.

Процедура проверки будет запущена через 48 часов, если мы не получим от вас возражений. Во время этой процедуры наша команда продолжит работать с вашим запросом.

С уважением,

{{ incident handler name }}


* * *


### Comments

Если вопрос касается онлайновой гендерной агрессии, пожалуйста, используйте шаблон из [Article #269: Inform Clients Targeted by Harassment about Vetting Process](269-Vetting_Info_Clients_Harassment.html)


* * *


### Related Articles

- [Article #269: Inform Clients Targeted by Harassment about Vetting Process](269-Vetting_Info_Clients_Harassment.html)
