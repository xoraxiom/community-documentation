---
title: Reboot a Website under OVH Using FileZilla
keywords: website management, website down, reboot, FTP, OVH, FileZilla
last_updated: November 7, 2019
tags: [infrastructure, articles]
summary: "A website hosted under OVH was shut down as a result of prohibited activity, hacking, or phishing. The client still has access to the FTP client, so the website can be rebooted."
sidebar: mydoc_sidebar
permalink: 151-Reboot_Website_OVH_FileZilla.html
folder: mydoc
conf: Public
ref: Reboot_Website_OVH_FileZilla
lang: en
---


# Reboot a Website under OVH Using FileZilla
## How to reboot a website which was shut down after prohibited activity 

### Problem

The website has been shut down as a result of prohibited activity, hacking, or phishing, and is still down.


* * *


### Solution

#### Pre-requisites

Make sure the client has the following:

- FTP login credentials for their website on OVH
- An FTP client. It is highly recommended to use [Filezilla](https://filezilla-project.org/).

#### Procedure

*Note: this procedure does not work in SFTP*

Guide the client through the following steps:

- Open FileZilla, click on “Server”, then select “Enter custom command”.

    *In FileZilla, you may see “Enter an FTP command” instead of “Enter custom command”.*

- In the new window that appears, enter the following command:

        SITE CHMOD 705 /

    If you see the following error:

    > 550 would not chance perms on /. not such file or directory

    You should use this command:

        SITE CHMOD 705 .
        
- To check that it is back online, just test the website in a web browser.



* * *


### Comments

Comment from the OVH support page:

- To check that the reboot has been successful, simply test your website in a web browser.

- Reminder: we recommend that you test that your website is displaying properly after 3 hours. Our robots monitor your website every 3 hours to check any change in permissions. Depending on when you make the changes, your site may display again fairly quickly.

- If the 3 hour deadline passes and your site is still not online, please contact the support team.

More on [this link](https://www.ovh.com/us/g1392.web_hosting_ovh_shutdown_procedure_after_website_hack#useful_information_reboot_via_filezilla).


* * *


### Related Articles

