---
title: Facebook - Page Blocked - Russian
keywords: email templates, page recovery, Facebook
last_updated: August 12, 2021
tags: [account_recovery_templates, templates]
summary: "Первое письмо клиенту, чья страница в Facebook заблокирована из-за использования псевдонима"
sidebar: mydoc_sidebar
permalink: 315-FB_Page_Blocked_ru.html
folder: mydoc
conf: Public
ref: FB_Page_Blocked
lang: ru
---


# Facebook - Page Blocked
## Initial email to client about a Facebook page blocked due to an alias

### Body

Здравствуйте,

Меня зовут {{ incident handler name }}. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now и постараюсь вам помочь.

Мы получили ваш запрос. Скажите, пожалуйста, вы уже пробовали сменить свое имя в Facebook на настоящее или вернуть себе доступ к заблокированному аккаунту через официальную форму Facebook (https://www.facebook.com/help/contact/260749603972907)?

Зная это, мы сможем составить оптимальный план действий.

С уважением,

{{ incident handler name }}
