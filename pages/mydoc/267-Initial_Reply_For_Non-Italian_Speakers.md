---
title: Initial Reply - For Non-Italian Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-Italian speakers"
sidebar: mydoc_sidebar
permalink: 267-Initial_Reply_For_Non-Italian_Speakers.html
folder: mydoc
conf: Public
lang: it
ref: ../Helpline_Procedures/FAQ-Initial_Reply.md
---


# Initial Reply - For Non-Italian Speakers
## First response, Email to Client if you're a non-Italian speaker

### Body


Gentile {{ beneficiary name }},

mi chiamo {{ incident handler name }} e faccio parte della Digital Security Helpline di Access Now - https://accessnow.org/help

Abbiamo ricevuto la sua richiesta {{ email subject }}, ma purtroppo non parlo italiano. Se la sua richiesta è urgente, risponda a questo messaggio aggiungendo "URGENT" all'oggetto della mail. In tal caso, ci occuperemo del suo caso il più rapidamente possibile.

Se crede che possiamo proseguire questa conversazione in [[ en ]] inglese o in [[ fr ]] francese [[ de ]] tedesco/ [[ sp ]] spagnolo [[ pt ]] portoghese [[ ar ]] arabo [[ ru ]] russo, può rispondermi direttamente.

Le persone del nostro team che parlano italiano entreranno in servizio tra poche ore e si occuperanno del suo caso.

Cordiali saluti,

{{ incident handler name }}


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
