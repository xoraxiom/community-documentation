---
title: Vetting Request -  Encrypted
keywords: email templates, vetting, vetting process, partner
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "External Vetting, email to partner (only encrypted)"
sidebar: mydoc_sidebar
permalink: 10-Vetting_Request-Encrypted.html
folder: mydoc
conf: Public
ref: Vetting_Request-Encrypted
lang: en
---


# Vetting Request -  Encrypted
## External Vetting, email to partner (only encrypted)

### Body

Dear {{ beneficiary name }},

My name is {{ incident handler name }} and I’m part of Access Now Digital Security Helpline. I received your contact information from [[ Access Now Employee's or Partner's Name (Not the client information) ]]. I’m contacting you to ask for assistance with the vetting process of a new Helpline client.

Recently we received a help request from [[ {{ beneficiary email }}, {{ beneficiary signal number }}, Position, Organization ]]. They are requesting our guidance for a digital security concern.

We would be thrilled to continue providing our help, but first need to verify with trusted partners their identity and work. It would be of great help for us if you could verify their identity and confirm they are a trustworthy member of civil society.

Could you please confirm that:

- This contact information is associated with the person in question
- You trust this person in general and believe their actions do not negatively affect civil society

If you have any question or concern please let us know. In advance I appreciate your kind assistance.

{{ incident handler name }}
