---
title: Website Down
keywords: CMS, website, Drupal, Joomla, WordPress, Jekyll, SSG, static site generators, framework
last_updated: February 10, 2021
tags: [web_vulnerabilities, articles]
summary: "A website is unreachable and the client needs to understand why and what to do"
sidebar: mydoc_sidebar
permalink: 171-Website_down.html
folder: mydoc
conf: Public
lang: en
---


# Website Down
## How to track an issue when a website is down

### Problem

A website is unreachable and the client needs assistance in understanding what is going on and how to address the issue.


* * *


### Solution

- Check if the website is online from your end, as it could be censored in the client country. You can use http://www.isup.me/
- Try accessing the website using [Tor Browser](https://www.torproject.org/). If it's accessible from Tor Browser, this may indicate [the website is being blocked in a certain country](https://support.torproject.org/censorship/#censorship-1). If it's inaccessible, this may indicate that [the website is blocking connections through Tor Browser](https://support.torproject.org/censorship/#censorship-2).
- Check the website of the host provider, as it's possible the provider is facing some technical issues.
- Check if there is any error message. It could be a software issue.
- It could be a campaign against a type of websites that support the same cause. Check the availability of other websites.

Some tests that can be done to track the issue:

* ping the website and see if ping returns an IP or not.
* nmap the website to find which ports are working.
* Whois the website to identify the location of the admin, the host provider, etc.
* Reverse its IP to find out if the website is co-hosted.
* If the site is co-hosted, find out if the other websites are down as well.

Based on the results of these tests, we can figure out if the connection
request is being turned down by the ISP, censored by the authorities, or if
the website is down due to a DoS attack, etc.


* * *


### Comments

#### References

- [Digital First Aid Kit troubleshooting guide](https://www.digitalfirstaid.org/en/topics/website-not-working/)
- [What to do when your website goes down](https://github.com/OpenInternet/MyWebsiteIsDown/blob/dev/MyWebsiteIsDown.md)
