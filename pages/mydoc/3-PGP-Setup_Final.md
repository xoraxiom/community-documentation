---
title: PGP - Setup - Final Email
keywords: PGP setup, digital security basics
last_updated: August 12, 2021
tags: [secure_communications_templates, templates]
summary: "Email to client to confirm that they have set up PGP"
sidebar: mydoc_sidebar
permalink: 3-PGP-Setup_Final.html
folder: mydoc
conf: Public
ref: PGP-Setup_Final
lang: en
---


# PGP - Setup - Final Email
## Email to client to confirm that they have set up PGP

### Body

Hi {{ beneficiary name }},

It seems that the PGP setup for your email account is completed. Now that you finished the installation process, I would like to provide some recommendations regarding the usage of PGP:

- Please make sure to keep your private key safe. Avoid making copies of it, and keep just one backup in an encrypted USB stick that you keep somewhere safe.

- Store your revocation certificate in an encrypted storage device that you keep in a safe location, or print it out and hide it in a safe place.

- If you ever get asked by a prompt if you want to encrypt your drafts, click yes - you don't want to keep unencrypted copies of the emails in your folders.

- Don't forget about the security of your computer. Make sure to update your system and be cautious not to click on suspicious links and not to open unsolicited attachments.

- When writing to help@accessnow.org please remember to disable the subject line encryption. You can find instructions in our guide for PGP: [[ Windows: [https://guides.accessnow.org/PGP_Encrypted_Email_Windows.html#subject-encryption](https://guides.accessnow.org/PGP_Encrypted_Email_Windows.html#subject-encryption) ]] - [[ Mac: [https://guides.accessnow.org/PGP_Encrypted_Email_Mac.html#subject-encryption](https://guides.accessnow.org/PGP_Encrypted_Email_Mac.html#subject-encryption) ]] - [[ Linux: [https://guides.accessnow.org/PGP_Encrypted_Email_Linux.html#subject-encryption](https://guides.accessnow.org/PGP_Encrypted_Email_Linux.html#subject-encryption) ]]

Don't hesitate to contact me or one of my colleagues at Access Now Helpline if you ever have any question about PGP, or any other security issues.

[[ if the client is on Gmail ]]

You can start by applying those best practices advised by Google for Thunderbird :
https://support.google.com/mail/answer/78892?hl=en


[[ facultative : extend email encryption to your phone ]]

We would not recommend it, but if you absolutely need to decrypt and encrypt your email in your phone, you can do it. If you decide to set up PGP on your mobile device, please get in touch with us before you proceed, so we can help set up your phone correctly.

Congratulations for your success in securing your communications!

Kind regards,

{{ incident handler name }}


* * *


### Related Articles
