---
title: "Documentation de la communauté de la plateforme d’assistance numérique d'Access Now"
keywords: home
tags: [home]
layout: home
sidebar: mydoc_sidebar
permalink: fr_index.html
ref: home
lang: fr
---


Dans ce site, vous trouverez des guides que nous utilisons pour la plateforme d’assistance numérique d'Access Now afin de traiter différents types de cas au cours de nos opérations 24/7.

Si vous gérez un centre d'assistance en sécurité numérique, si vous souhaitez simplement aider vos amis à sécuriser leur communication en ligne, ou si vous avez besoin de réagir à un incident de sécurité numérique, vous trouverez des conseils et des procédures utiles dans ces guides.

Si vous souhaitez mettre à jour une page, vous pouvez simplement cliquer sur le bouton Modifier ou visiter cette [repository Gitlab](https://gitlab.com/AccessNowHelpline/community-documentation).

{% include links.html %}


