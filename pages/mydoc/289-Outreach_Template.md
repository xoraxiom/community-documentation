---
title: Outreach Template
keywords: email templates, outreach
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Email template for outreach - addressed at members of civil society"
sidebar: mydoc_sidebar
permalink: Outreach_Template.html
folder: mydoc
conf: Public
ref: Outreach_Template
lang: en
---


# Outreach Template
## Email template for outreach - addressed at members of civil society

### Body

Dear friends,

It's a pleasure to introduce Access Now Digital Security Helpline, a free of charge resource that offers real-time, direct technical assistance and advice to users at-risk around the world.

We know that many civil society members in [[ region/sector ]] are facing particular digital security attacks [[ description ]], and we would like to help.

Among the services the Helpline provides to independent media, journalists, bloggers, human rights defenders, activists and other civil society organizations, are:

- Rapid response on digital security incidents.
- Personalized recommendations, instructions and follow-up on digital security issues.
- Guidance on the usage of digital security tools.
- Support for securing technical infrastructure, websites, and social media against attacks.

The service is provided 24/7 all year round. If you have any personal or organizational digital security issues you want to raise with us, whether connected or not to the current situation, please go ahead and email us at help@accessnow.org. You can encrypt your email by using this PGP key: https://pgp.mit.edu/pks/lookup?op=get&search=0xC46BED3332E8A2BC

We will be pleased to provide our support in the effort of securing your digital activities on- and off-line. Please note that the service is free and confidential.

Best regards,

{{ incident handler name }}
