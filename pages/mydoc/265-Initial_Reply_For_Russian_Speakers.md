---
title: Initial Reply - For Russian Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Первый ответ, письмо клиенту от русскоговорящего сотрудника"
sidebar: mydoc_sidebar
permalink: 265-Initial_Reply_For_Russian_Speakers.html
folder: mydoc
conf: Public
lang: ru
---


# Initial Reply
## First response, Email to Client for Russian speakers

### Body

Здравствуйте,

Спасибо за обращение в Службу поддержки по вопросам цифровой безопасности организации Access Now (https://www.accessnow.org/help). Меня зовут {{ incident handler name }}.

Мы получили ваш запрос. Наша команда работает над ним.

Специалисты службы поддержки находятся в разных временных зонах. Вам может ответить другой участник команды. Это зависит от того, в какой день и в какое время приходит ваше сообщение. В ближайшее время мы свяжемся с вами, чтобы обсудить ваш запрос подробнее.

Пожалуйста, оставляйте “[accessnow #{{ ticket id }}]” в теме письма для всех сообщений по вашему запросу. Это идентификационный номер, автоматически созданный нашей системой.

С уважением,

{{ incident handler name }}


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
