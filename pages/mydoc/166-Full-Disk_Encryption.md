---
title: FAQ - Full-Disk Encryption (FDE)
keywords: FDE, full-disk encryption, device security, mobile security
last_updated: October 7, 2021
tags: [devices_data_security, articles, faq]
summary: "A client's sensitive information is stored on a device (laptop, smartphone) unprotected; a client has critical data on their device and is concerned about loss or theft."
sidebar: mydoc_sidebar
permalink: 166-Full-Disk_Encryption.html
folder: mydoc
conf: Public
ref: Full-Disk_Encryption
lang: en
---


# Full-Disk Encryption (FDE)
## Secure sensitive information through full-disk encryption

### Problem

- Private/sensitive data stored in a device need to be protected from access by unauthorized persons.


* * *


### Solution

#### Warnings

1. Before proceeding with full-disk encryption, the client should have the root credentials to the device, and make a backup of all data.

2. Make the client aware of the risks. Note the following concerns:
    - The client should keep in mind that malware could defeat the encryption by waiting to copy files off of their computer until after booting, when they enter the decryption password.

    - The encrypted data is only protected when the computer is off. If an adversary gets access to the computer while it is on, in sleep mode, or hibernated, there are several techniques that can be used to extract data.

    - Encryption is only as good as the encryption password. If the attacker has the user's device, they have all the time in the world to try out new passwords.

    - While encryption can prevent casual access, the client should preserve truly confidential data by keeping it hidden to prevent any physical access, or cordoned away on a much more secure machine. The client should not trust their encrypted machine implicitly.

* * *

What follows is a list of full-disk encryption tools, listed by device type and operating system:


#### Windows

* **BitLocker**

    An included option for Windows Ultimate and Enterprise editions of *Windows Vista* and *Windows 7*, the Pro and Enterprise editions of *Windows 8* and *Windows 8.1*, and Windows Server 2008, Windows 10,and later.

    - [Windows 7](https://technet.microsoft.com/en-us/library/ee424299(v=ws.10).aspx)

    - [Windows 8](https://support.microsoft.com/en-us/help/2855131/how-to-enable-bitlocker-device-encryption-on-windows-8-rt)

    - [Windows 10](https://support.microsoft.com/en-us/help/4028713/windows-10-turn-on-device-encryption)

    - [Guide on Security in a Box](https://securityinabox.org/en/guide/basic-security/windows/#windows-full-disk-encryption-with-bitlocker)

* **Device Encryption**

    An included option for Windows Home edition of *Windows 8.1* and *Windows 10* on devices with TPM 2.0 and Modern Standby (also known as Connected Standby and InstantGo) hardware support. Device Encryption is the same as Bitlocker, with the limitation that users have no option to configure it and there is no preboot-authentication.

    - [Windows 10](https://support.microsoft.com/en-us/help/4502379/windows-10-device-encryption)

*BitLocker Device Encryption is enabled automatically so that the device is always protected. [This page outlines how this happens](https://docs.microsoft.com/en-us/windows/security/information-protection/bitlocker/bitlocker-device-encryption-overview-windows-10#bitlocker-device-encryption).*

*BitLocker and Device Encryption are not available in all Windows versions. If the version running on the client's operating system does not include Bitlocker or Device Encryption,
we can suggest them to create an encrypted folder for sensitive files with [VeraCrypt](https://www.veracrypt.fr/en/Home.html), or to use Diskcryptor following the instructions in
[Article #78: FDE with DiskCryptor on Windows](78-fde-diskcryptor-win.html).*
#### macOS

* **FileVault 2**

    An included option with Mac OS X Lion and later. Please review [Article #104: Full-Disk Encryption on Mac with FileVault 2](104-FDE_Mac_FileVault2.html) for more details and links to end-user guides.

    For older versions - Mac OS X Leopard and Mac OS X Snow Leopard - clients can use FileVault.


#### Linux

Full-disk encryption is usually an option that can be enabled during the installation of the system.

If the client is using Ubuntu (or another Debian-based distribution), and full-disk encryption hasn't been enabled during the installation, please review [Article #77: Ubuntu - Linux - FDE after OS installation](77-Ubuntu_Linux_FDE_After_Installation.html) for recommendations on disk encryption after installation.


#### Android

New devices running Android 10 and higher use [file-based encryption](https://source.android.com/security/encryption/file-based) instead of full-disk encryption.

In Android 6 to 9, [full-disk encryption](https://source.android.com/security/encryption/full-disk) is enabled by default. To double-check that it is enabled, follow the steps listed below for older Android versions.

An explanation of the difference between file-based encryption (FBE) and full-disk encryption (FDE) can be found in [this article](https://www.hexnode.com/blogs/everything-you-need-to-know-about-android-encryption/).

In Android 4.0 Ice Cream to Android 5.0 Lollipop device encryption is an option. To enable it, go to: Settings -> Personal -> Security -> Encryption. Before enabling device encryption, however, a screen lock password needs to be set up.

*Note: Before starting the encryption process, ensure the phone is fully charged and plugged into a power source.*


#### iOS

* **Passcode**

    iPhones with iOS 8 and higher are encrypted by default, but if the client is concerned, we can double-check that it is enabled.

    Encryption is an option available on devices running iOS 4 to iOS 7, but it is not enabled by default. To enable it, follow these steps:

    1. Tap Settings > Passcode (or Touch ID and Passcode) > Turn Passcode On.
    2. Follow the prompts to create a passcode.
    3. After the passcode is set, scroll down to the bottom of the screen and look for the phrase "Data protection is enabled".
    4. From the same window you should set the “Require passcode” option to “Immediately”, so that your device isn't unlocked when you are not using it.

    If the client is using the iTunes option to back up the device on their computer, it will be necessary to choose the “Encrypt backup” option on the *Summary* tab of the device in iTunes. All data will be encrypted by iTunes before it's saved on the computer.

    If the client backs up to iCloud, according to Apple, the data is encrypted. Apple holds the encryption keys. Use a long passphrase to protect the data, and keep that passphrase safe.

    Finally, if the client has critical data on the device and is concerned about loss or theft, setting the device to wipe all data after 10 failed passphrase attempts is an option, as is remote erasure of the data. The client will need to activate the auto backup option first.


* * *


### Comments

In [this post](https://www.schneier.com/blog/archives/2015/06/encrypting_wind.html), Bruce Schneier explains why using Bitlocker is a reasonable solution on Windows.

* * *

### Related Articles

- [Article #104: Full-Disk Encryption on Mac with FileVault 2](104-FDE_Mac_FileVault2.html)
- [Article #77: Ubuntu - Linux - FDE after OS installation?](77-Ubuntu_Linux_FDE_After_Installation.html)
- [Article #78: FDE with DiskCryptor on Windows](78-fde-diskcryptor-win.html)
-
