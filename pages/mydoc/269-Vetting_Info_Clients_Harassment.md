---
title: Inform Clients Targeted by Harassment about Vetting Process
keywords: email templates, vetting, vetting process, harassment, gendered online violence
last_updated: August 12, 2021
tags: [helpline_procedures_templates, harassment_templates, templates]
summary: "Template to inform clients who have been targeted by gendered online violence about our vetting process"
sidebar: mydoc_sidebar
permalink: 269-Vetting_Info_Clients_Harassment.html
folder: mydoc
conf: Public
ref: Vetting_Info_Clients_Harassment
lang: en
---


# Inform Clients Targeted by Harassment about Vetting Process
## Template to inform clients who have been targeted by gendered online violence about our vetting process

### Body

Hi {{ beneficiary name }},

We appreciate your trust in the Helpline and we hope this message finds you well.
In order to be able to provide you some of our services and to extend our trust network, we will confirm your identity with the help of Access Now's trusted partners.

We will securely reach out to them and share your name and contact information to confirm your identity and that you are part of civil society. Any other information, including the reason you contacted the Helpline, will remain strictly confidential.

To confirm your identity, we are thinking of reaching out to the following organizations: [[ Vettor's Org Name - but not the vettor's name! ]] and [[ Vettor's Org Name - but not the vettor's name! ]]. Please let us know if you have reasons to believe that this might not be the best choice for your situation.

Please also note that Access Now’s Helpline services are not intended for individuals under the age of 18. If you are underage, please do let us know, and we will direct you to partners that can provide the support your require.

Our team will continue to work on your request while we complete this process. Please note that this process will be launched in 48 hours if you don't raise any objection.

You can help us move this process forward by filling out the following survey about you and the scope of your work:

https://form.accessnow.org/index.php/652353/Y?ticket={{ vetting ticket id }}

Regards,
{{ incident handler name }}


* * *


### Comments

Please remember not to mention the name of the individual vettor we are going to reach out to.
