---
title: Initial Reply - For Non-French Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for Non-French speakers"
sidebar: mydoc_sidebar
permalink: 220-Initial_Reply_For_Non-French_Speakers.html
folder: mydoc
conf: Public
lang: fr
---


# Initial Reply - For Non-French Speakers
## First response, Email to Client if you're a non-French speaker

### Body


Che(è)r(e) {{ beneficiary name }},

Mon nom est {{ incident handler name }}, et je fais partie de l’équipe de réponse aux incidents numériques de Access Now - https://www.accessnow.org/plateforme-dassistance-pour-la-securite-numerique.

J’ai bien reçu votre e-mail {{ email subject }}. Malheureusement, je ne pourrai pas m’exprimer en Français. Si votre demande est urgente, veillez répondre à cet e-mail et ajouter le mot “URGENT” au sujet et nous essayerons de vous répondre dans les plus brefs délais.

Si vous pouvez vous exprimer en Anglais ( ou en Allemand, ou en Espagnol, ou en Italien,  ou en Portugais, ou en Russe ) Nous serions capables de vous répondre directement maintenant.
Sinon, nos collègues qui s'expriment en Français seront dans en ligne dans quelques heures. Nous les attendrons.

Merci,

{{ incident handler name }}


* * *


### Related Articles

- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)
