---
title: Content Removal - Out of Mandate
keywords: harassment, content moderation, content removal, remove accounts, takedown, out of mandate
last_updated: August 12, 2021
tags: [harassment_templates, templates]
summary: "Template to communicate to the client that their request for content removal is out of mandate"
sidebar: mydoc_sidebar
permalink: 373-Content_Removal_Out_of_Mandate.html
folder: mydoc
conf: Public
ref: Content_Moderation_Out_of_Mandate
lang: en
---


# Content Removal - Out of Mandate
## Template to communicate to the client that their request for content removal is out of mandate

### Body

Dear {{ beneficiary name }},

My name is {{ incident handler name }}, and I'm part of the Access Now Digital Security Helpline. Thank you for reaching out.

While we understand the hardship you are going through, unfortunately, we cannot assist you with your request, as we have determined that it falls outside of the scope of assistance of the Digital Security Helpline. You can find our terms and the scope of our work described here: https://www.accessnow.org/helpline-terms-of-service/.

Also, please note that Access Now’s work includes recommendations to social media platforms to improve their response to content moderation issues, including ensuring that their decisions conform to the principles of transparency and proportionality, and that they provide direct access to an effective remedy for all users. The protection of your human rights, online and off, is the utmost priority for us. In keeping with this commitment, we strongly encourage platforms to comply with international human rights law when crafting and deploying their content moderation practices. You can read in detail about our work on human rights-based approach to content governance here: https://www.accessnow.org/digital-security-helpline-our-approach-to-content-related-cases/.

If you would like to provide feedback on the Digital Security Helpline please feel free to follow this survey:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={{ ticket id }}

Kind regards,

{{ incident handler name }}


* * *


### Related Articles

- [Article #371: Content Moderation - Requests for Content Removal](371-content_removal.html)
