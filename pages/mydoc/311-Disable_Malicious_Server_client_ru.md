---
title: Disable C&amp;C Server - Email to Client - Russian
keywords: C&amp;C server, malware, email templates, malicious website
last_updated: August 12, 2021
tags: [vulnerabilities_malware_templates, templates]
summary: "Письмо клиенту или партнеру с просьбой разрешить распространение данных о вредоносном коде для других защитников цифровых прав после обезвреживания сервера управления"
sidebar: mydoc_sidebar
permalink: 311-Disable_Malicious_Server_client_ru.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server_client
lang: ru
---


# Disable C&amp;C Server - Email to Client
## Template for ask the client for permission to share the malware indicators with the digital security community

### Body

Здравствуйте,

Меня зовут {{ incident handler name }}. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now (https://accessnow.org/help). В настоящее время мы обрабатываем ваш запрос. Нам нужна информация, которая поможет решить проблему.

Прошу вашего разрешения на передачу симптомов вредоносного кода и его сервера управления (C&amp;C) пользователям нашего сообщества. Информация о вредоносном коде и симптомах снизит общий урон и поможет защитить тех, кто может оказаться следующей жертвой.

Ждем вашего ответа, чтобы продолжить работу по запросу.

С уважением,

{{ incident handler name }}


* * *


### Related Articles
