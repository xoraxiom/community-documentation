---
title: Vetting Request - Encrypted for Spanish Speakers
keywords: email templates, vetting, vetting process, partner
last_updated: August 12, 2021
tags: email templates, vetting, vetting process, partner
summary: "External Vetting, email to partner (only encrypted) for Spanish Speakers"
sidebar: mydoc_sidebar
permalink: 400-Vetting_Request-Encrypted_Spanish.html
folder: mydoc
conf: Public
ref: Vetting_Request-Encrypted-Spanish
lang: es
---


# Vetting Request -  Encrypted for Spanish Speakers
## External Vetting, email to partner (only encrypted)

### Body

Estimada/o {{ beneficiary name }}

Mi nombre es {{ incident handler name }} y soy parte de la Línea de Ayuda en Seguridad Digital de Access Now.  Recibimos la información de su contacto a través de [[ Access Now Employee's or Partner's Name (Not the client information) ]]. Le estoy contactando para solicitarle ayuda con la verificación de una persona. Recientemente nos contactó [[ XXXX name@domain, [Signal #], Position, Organization) ]] y está solicitando nuestra asistencia en un tema de seguridad digital.

Nos encantaría continuar ayudando en este caso, pero primero que todo debemos verificar con personas de confianza la identidad de esta persona y el trabajo que realiza. Sería de gran ayuda si nos pudiera ayudar verificando su identidad y confirmando que esta persona es miembro de confianza de la sociedad civil.

Podría confirmar si:

 - Esta información de contacto está asociada a la persona en cuestión
 - En términos generales, usted confía en esta persona y en que sus acciones no afectan negativamente a la sociedad civil

Si tiene alguna pregunta o inquietud por favor indíquenosla. De antemano le agradezco mucho su ayuda.

Saludos cordiales,

{{ incident handler name }}



* * *


### Related Articles

[Article #10: FAQ - Vetting Request - Encrypted](https://git.accessnow.org/access-now-helpline/faq/-/blob/master/Templates/10-Vetting_Request-Encrypted.md)
