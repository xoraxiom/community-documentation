---
title: PGP - Setup - Final Email - French
keywords: PGP setup, digital security basics
last_updated: August 12, 2021
tags: [secure_communications_templates, templates]
summary: "Email to French-speaking client to confirm that they have set up PGP"
sidebar: mydoc_sidebar
permalink: 185-PGP-Setup_Final_fr.html
folder: mydoc
conf: Public
lang: fr
---


# PGP - Setup - Final Email - French
## French email to client to confirm that they have set up PGP

### Body

Cher/Chère {{ beneficiary name }},

La configuration PGP de votre compte email est terminée. Maintenant que vous avez terminé le processus d'installation, je voudrais vous donner quelques recommandations concernant l'utilisation de PGP:

- S'il vous plaît, prenez soin de votre clé PGP privée et de votre certificat de révocation. Évitez d'en faire des copies, et essayez de garder juste une sauvegarde dans une clé USB que vous garderez toujours sur vous.

- Par défault, Thunderbird garde des copies de vos emails dans votre disque dur. A moins que vous voulez changer ça, nous ne conseillons pas de conserver des copies non chiffrées des e-mails dans votre machine locale. Pour cela, nous vous conseillons de crypter votre disque dur. Nous pouvons vous assister pour ça bien sûr.

- N'oubliez pas la sécurité de votre ordinateur. Assurez-vous de mettre à jour votre système d'exploitation, d'activer le pare-feu d'avoir un antivirus. Enfin, vous devez être prudent avec les hyperliens et les pièces jointes non sollicitées.

Enfin, pour optimiser votre utilisation de Thunderbird, vous pouvez consulter le lien ci-dessous, c'est un article qui liste les meilleures configuration pour Thunderbird avec Gmail: https://support.google.com/mail/answer/78892?hl=en

Bien sûr, pour toute nouvelle demande, vous pouvez nous contacter via notre adresse email: help@accessnow.org.

Merci,

{{ incident handler name }}



* * *


### Related Articles

- [Article #3: PGP - Setup - Final Email](3-PGP-Setup_Final.html)
