---
title: Russian - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 17, 2022
tags: [helpline_procedures_templates, templates]
summary: "Шаблон письма для сбора обратной связи от клиентов"
sidebar: mydoc_sidebar
permalink: 350-ru_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: ru
---


# Russian - Feedback Survey Template
## Шаблон письма для сбора обратной связи от клиентов

### Body

Здравствуйте, {{ beneficiary name }}.

Благодарим за обращение в Службу поддержки по цифровой безопасности, организованную международной правозащитной организацией Access Now - https://accessnow.org.

Уведомляем, что Ваше обращение "{{ email subject }}" закрыто.

Ваше мнение очень важно для нас. Если вы хотите поделиться своими пожеланиями по взаимодействию со Службой поддержки Access Now, пожалуйста, примите участие в опросе:

https://form.accessnow.org/index.php/139723?lang=ru&139723X20X841={{ ticket id }}

Номер Вашего обращения: {{ ticket id }}

Если у Вас есть какие-либо вопросы или комментарии - сообщите нам, и мы постараемся Вам помочь.

Спасибо,

{{ incident handler name }}
